#!/bin/bash
#SBATCH --job-name=zgemm
#SBATCH --output=par.out
#SBATCH --error=par.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=48
#SBATCH --time=00:30:00
#SBATCH --qos=debug
#SBATCH --exclusive

module load intel/2020.1 mkl/2021.4

M=1281 N=1281 K=38400 ./zgemm_ex &> "out.txt"
