ifeq ($(MKL_HOME),)
ifeq ($(MKLROOT),)
$(error Please set MKL_HOME or MKLROOT)
else
MKL_HOME=$(MKLROOT)
endif
endif

FC=ifort
FFLAGS=-O3 -qopenmp -I$(MKL_HOME)/include/ -I$(MKL_HOME)/include/intel64/lp64
LIBS=-L$(MKL_HOME)/lib/intel64/

LIBS += -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core
LIBS += -liomp5 -lpthread
zgemm_ex: zgemm.F90
	$(FC) $(FFLAGS)  -o $@ $< $(LIBS)

clean:
	@rm zgemm_ex 2> /dev/null || true
