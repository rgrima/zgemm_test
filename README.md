# Zgemm Test

Test MKL zgemm

## Getting started

It runs a ZGEMM using the parallel version of MKL, from 1 thread to the maximum number of available threads.

For every test, the first iteration is discarded.

## Running the experiment

Load needed modules, compile and set matrix sizes in environment variables
```bash
module load intel/2018.4 impi/2018.4 mkl/2018.4
make
M=1000 N=1000 K=10000 ./zgemm_ex
```

In order to run in a queue system, modify and execute file 'run.sh':
```bash
./run.sh
```

If you want to run several experiments and get some graphs, modify and execute file 'do.sh':
```bash
./do.sh
```

Once the the execution has finished you can get the graphs:
```bash
./plot.py
```


## Results in Marenostrum

![My Image](IMG/01000.01000.10000.png)
![My Image](IMG/10000.10000.01000.png)
