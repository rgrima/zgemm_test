#!/bin/bash
#SBATCH --job-name=zgemm
#SBATCH --nodes=1
#SBATCH --output=zgemm.out
#SBATCH --time=00:30:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=112
#SBATCH --qos=gp_debug
#SBATCH --account=bsc21
#SBATCH --error=zgemm.err



#cpupower frequency-info

#./cpu_frequencies.py > my_freq &

module purge
module load oneapi/2023.2.0

sec=1
while true; do
    [ -f out.txt ] && {
        nth=$( grep "subroutine on" out.txt | tail -n 1 | cut -c 34-37 )
        [ "$nth" -eq  "$nth" ] &> /dev/null && {
            frq=$( cpupower frequency-info --freq | tail -n 1 | cut -c 25-33 )
            echo "$nth $frq"
        }
        #[ $nth -gt 24 ] && sec=1 || { [ $nth -gt 16 ] && sec=2 || { [ $nth -gt 8 ] && sec=3 || sec=4; } }
    }
    sleep $sec
    #echo "$( grep "to use"  out.txt | tail -n 1 | cut -c 32-35 ) $( cpupower frequency-info --freq | tail -n 1 | cut -c 25-33 )"
done > my_freq  &
M=1281 N=1281 K=38400 ./zgemm_ex &> "out.txt"

#kill -9 -1
