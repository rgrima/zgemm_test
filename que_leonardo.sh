#!/bin/bash
#SBATCH --job-name=LVL_TRI
#SBATCH --nodes=1
#SBATCH --output=OUTPUT.leonardo-intel_vec
#SBATCH --time=00:30:00
#SBATCH --ntasks=32
#SBATCH --partition boost_usr_prod
#SBATCH --account=Max3_devel_2
##SBATCH --cpus-per-task=1
##SBATCH --threads-per-core=1
##SBATCH --exclusive
##SBATCH --error=job.err

#cpupower frequency-info

#./cpu_frequencies.py > my_freq &

module load intel-oneapi-mkl/2023.2.0 intel-oneapi-compilers/2023.2.1

sec=1
while true; do
    [ -f out.txt ] && {
        nth=$( grep "subroutine on" out.txt | tail -n 1 | cut -c 34-37 )
        [ "$nth" -eq  "$nth" ] &> /dev/null && {
            frq=$( cpupower frequency-info --freq | tail -n 1 | cut -c 25-33 )
            echo "$nth $frq"
        }
        #[ $nth -gt 24 ] && sec=1 || { [ $nth -gt 16 ] && sec=2 || { [ $nth -gt 8 ] && sec=3 || sec=4; } }
    }
    sleep $sec
    #echo "$( grep "to use"  out.txt | tail -n 1 | cut -c 32-35 ) $( cpupower frequency-info --freq | tail -n 1 | cut -c 25-33 )"
done > my_freq  &
M=1281 N=1281 K=38400 ./zgemm_ex &> "out.txt"

#kill -9 -1
