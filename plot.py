#!/usr/bin/env python3

from os       import listdir
from os.path  import join, isfile, isdir, getsize
import matplotlib.pyplot as plt
from   matplotlib.gridspec import GridSpec
import numpy as np
from bisect import bisect

DIR="WDIR"
base='out.'

Data = {}
Size = []
for di in sorted( listdir( DIR ) ) :
    wdir = join(DIR,di)
    for f in sorted( listdir( wdir ) ) :
        if not f.startswith( base ) : continue
        s = f.split(".",1)[1]
        if s not in Size : Size.append( s )
        fn = join( wdir, f )
        TIMES=[]
        for l in open(fn).readlines() :
            if "completed at" in l : TIMES.append( float(l.split()[3])/1000 )
            if "== using" in l :
                th=int(l.split()[2])
        Data[s] = np.array(TIMES)

if True :
    for tag in Data.keys() :
        print( tag )
        plt.figure().clear()
        t=Data[tag]
        #if x is None :
        x=np.arange(1,t.size+1)
        sp=t[0]/t
        plt.plot(x,sp)
        plt.fill_between( x, x,x.size*[1], color='lightgreen',alpha=0.4)
        plt.xlabel('Total # threads')
        plt.ylabel('Speedup')
        plt.title( "ZGEMM M=%d N=%d K=%d"%( tuple( [ int(i) for i in tag.split('.') ] ) ) )
        x1,x2,y1,y2 = plt.axis()
        plt.axis( [ x1,x2,y1,t.size ] )
        #plt.legend(loc="upper left")
        plt.show()
        #plt.savefig( tag+'.png' )

