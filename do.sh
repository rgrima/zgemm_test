#!/bin/bash

WDIR="WDIR"

# Needed modules to compile and execute the experiment
MODULES="intel/2018.4 impi/2018.4 mkl/2018.4"

# Matrix sizes to test: A(M,K), B(K,N) and C(M,N)
# We run different experiments, changing matrix sizes
M=( "500" "1000" "1000" "5000" "1000" "10000" )
N=( "500" "1000" "1000" "5000" "1000" "10000" )
K=( "1000" "500" "5000" "1000" "10000" "1000" )

# Max time limit for every experiment
T=( "00:05:00" "00:05:00" "00:10:00" "00:20:00" "00:30:00" "00:50:00" )


n=${#M[@]}

Queue=$( cat << EOF
#!/bin/bash
#SBATCH --job-name=zgemm
#SBATCH --output=par.out
#SBATCH --error=par.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=48
#SBATCH --time=_TIME_
#SBATCH --qos=debug
#SBATCH --exclusive

module purge &> /dev/null
module load intel/2018.4 impi/2018.4 mkl/2018.4
M=_M_ N=_N_ K=_K_ ./zgemm_ex &> \$( printf "out.%05i.%05i.%05i" _M_ _N_ _K_ )
EOF
)

[ -d "${WDIR}" ] && rm -rf ${WDIR}; mkdir ${WDIR}

for i in $( seq 0 $(( n-1 )) ); do
    wdir=$( printf "${WDIR}/ex-%02d" $(( i+1 )) )
    mkdir ${wdir}
    cp Makefile zgemm.F90 ${wdir}
    pushd ${wdir} &> /dev/null
    module purge &> /dev/null
    module load intel/2018.4 impi/2018.4 mkl/2018.4
    make
    Queue_t="${Queue/_TIME_/${T[$i]}}"
    Queue_t="${Queue_t//_M_/${M[$i]}}"
    Queue_t="${Queue_t//_N_/${N[$i]}}"
    Queue_t="${Queue_t//_K_/${K[$i]}}"
    echo "${Queue_t}" > que.sh; chmod +x que.sh
    sbatch que.sh
    popd &> /dev/null
done
